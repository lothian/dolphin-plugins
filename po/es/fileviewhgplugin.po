# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Eloy Cuadra <ecuadra@eloihr.net>, 2011, 2012, 2015, 2016, 2017, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2022-10-17 17:57+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: backoutdialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Backout"
msgstr "Deshacer de <application>Hg</application>"

#: backoutdialog.cpp:33
#, kde-kuit-format
msgctxt "@action:button"
msgid "Backout"
msgstr "Deshacer"

#: backoutdialog.cpp:60
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Merge with old dirstate parent after backout"
msgstr "Fusionar con el archivo «dirstate» padre anterior tras deshacer"

#: backoutdialog.cpp:62 backoutdialog.cpp:64 bundledialog.cpp:57
#, kde-kuit-format
msgctxt "@label:button"
msgid "Select Changeset"
msgstr "Seleccionar conjunto de cambios"

#: backoutdialog.cpp:68
#, kde-kuit-format
msgctxt "@label"
msgid "Revision to Backout: "
msgstr "Revisión a deshacer: "

#: backoutdialog.cpp:73
#, kde-kuit-format
msgctxt "@label"
msgid "Parent Revision (optional): "
msgstr "Revisión padre (opcional): "

#: backoutdialog.cpp:139 bundledialog.cpp:189
#, kde-kuit-format
msgctxt "@title:window"
msgid "Select Changeset"
msgstr "Seleccionar conjunto de cambios"

#: backoutdialog.cpp:140 bundledialog.cpp:190
#, kde-kuit-format
msgctxt "@action:button"
msgid "Select"
msgstr "Seleccionar"

#: branchdialog.cpp:24
#, kde-format
msgctxt "@title:window"
msgid "<application>Hg</application> Branch"
msgstr "Rama de <application>Hg</application>"

#: branchdialog.cpp:37
#, kde-format
msgid "Create New Branch"
msgstr "Crear nueva rama"

#: branchdialog.cpp:38
#, kde-format
msgid "Switch Branch"
msgstr "Cambiar rama"

#: branchdialog.cpp:70
#, kde-format
msgid "<b>Current Branch: </b>"
msgstr "<b>Rama actual: </b>"

#: branchdialog.cpp:108 branchdialog.cpp:123 tagdialog.cpp:102
#: tagdialog.cpp:118 tagdialog.cpp:133
#, kde-format
msgid "Some error occurred"
msgstr "Ha ocurrido algún error"

#: bundledialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Bundle"
msgstr "Empaquetado de <application>Hg</application>"

#: bundledialog.cpp:32
#, kde-kuit-format
msgctxt "@action:button"
msgid "Bundle"
msgstr "Empaquetar"

#: bundledialog.cpp:59
#, kde-kuit-format
msgctxt "@label"
msgid "Base Revision (optional): "
msgstr "Revisión base (opcional): "

#: bundledialog.cpp:61
#, kde-kuit-format
msgctxt "@label"
msgid "Bundle all changesets in repository."
msgstr "Empaquetar todos los conjuntos de cambios del repositorio."

#: bundledialog.cpp:76 exportdialog.cpp:55 importdialog.cpp:61
#: pulldialog.cpp:44 pushdialog.cpp:44
#, kde-format, kde-kuit-format
msgctxt "@label:group"
msgid "Options"
msgstr "Opciones"

#: bundledialog.cpp:78
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Run even when the destination is unrelated (force)"
msgstr "Ejecutar incluso cuando el destino no esté relacionado (forzar)"

#: bundledialog.cpp:81 pulldialog.cpp:40 pushdialog.cpp:40
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Do not verify server certificate"
msgstr "No verificar el certificado del servidor"

#: clonedialog.cpp:33
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Clone"
msgstr "Clonación de <application>Hg</application>"

#: clonedialog.cpp:34
#, kde-kuit-format
msgctxt "@action:button"
msgid "Clone"
msgstr "Clonar"

#: clonedialog.cpp:42
#, kde-format
msgid "URLs"
msgstr "URL"

#: clonedialog.cpp:44
#, kde-kuit-format
msgctxt "@label"
msgid "Source"
msgstr "Origen"

#: clonedialog.cpp:45
#, kde-kuit-format
msgctxt "@lobel"
msgid "Destination"
msgstr "Destino"

#: clonedialog.cpp:46 clonedialog.cpp:47
#, kde-kuit-format
msgctxt "@button"
msgid "Browse"
msgstr "Explorar"

#: clonedialog.cpp:59
#, kde-format
msgctxt "@label"
msgid "Options"
msgstr "Opciones"

#: clonedialog.cpp:62
#, kde-format
msgid "Do not update the new working directory."
msgstr "No actualizar el nuevo directorio de trabajo."

#: clonedialog.cpp:63
#, kde-format
msgid "Use pull protocol to copy metadata."
msgstr "Usar el protocolo de extracción para copiar metadatos."

#: clonedialog.cpp:64
#, kde-format
msgid "Use uncompressed transfer."
msgstr "Usar transferencias sin comprimir."

#: clonedialog.cpp:65
#, kde-format
msgid "Do not verify server certificate (ignoring web.cacerts config)."
msgstr ""
"No verificar el certificado del servidor (ignorar configuración web.cacerts)."

#: clonedialog.cpp:158
#, kde-format
msgid "Terminating cloning!"
msgstr "Terminando la clonación"

#: clonedialog.cpp:184
#, kde-kuit-format
msgctxt "@action:button"
msgid "Close"
msgstr "Cerrar"

#: clonedialog.cpp:189
#, kde-kuit-format
msgctxt "@message:error"
msgid "Error Cloning Repository!"
msgstr "Ha ocurrido un error al clonar el repositorio"

#: commitdialog.cpp:32
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Commit"
msgstr "Envío de <application>Hg</application>"

#: commitdialog.cpp:34
#, kde-kuit-format
msgctxt "@action:button"
msgid "Commit"
msgstr "Enviar"

#: commitdialog.cpp:41
#, kde-format
msgid ""
"The KTextEditor component could not be found;\n"
"please check your KDE Frameworks installation."
msgstr ""
"No se ha podido encontrar el componente KTextEditor.\n"
"Compruebe su instalación de KDE Frameworks."

#: commitdialog.cpp:54
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Commit to current branch"
msgstr "Enviar a la rama actual"

#: commitdialog.cpp:59
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Create new branch"
msgstr "Crear nueva rama"

#: commitdialog.cpp:64
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Close current branch"
msgstr "Cerrar rama actual"

#: commitdialog.cpp:86
#, kde-format
msgid "Copy Message"
msgstr "Copiar mensaje"

#: commitdialog.cpp:87 updatedialog.cpp:34
#, kde-format
msgid "Branch"
msgstr "Rama"

#: commitdialog.cpp:108
#, kde-kuit-format
msgctxt "@title:group"
msgid "Commit Message"
msgstr "Mensaje de envío"

#: commitdialog.cpp:115
#, kde-kuit-format
msgctxt "@title:group"
msgid "Diff/Content"
msgstr "Diferencias/contenido"

#: commitdialog.cpp:217
#, kde-format
msgid "Could not create branch! Aborting commit!"
msgstr "No se ha podido crear la rama. Envío interrumpido."

#: commitdialog.cpp:227
#, kde-format
msgid "Commit unsuccessful!"
msgstr "Envío sin éxito"

#: commitdialog.cpp:231
#, kde-format
msgid "No files for commit!"
msgstr "No hay archivos a enviar."

#: commitdialog.cpp:258
#, kde-format
msgid "Branch: Current Branch"
msgstr "Rama: Rama actual"

#: commitdialog.cpp:265
#, kde-format
msgid "Branch: "
msgstr "Rama: "

#: commitdialog.cpp:278
#, kde-format
msgid "Branch: Close Current"
msgstr "Rama: Cerrar actual"

#: commitdialog.cpp:291
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Commit: New Branch"
msgstr "Envío de <application>Hg</application>: Nueva rama"

#: commitdialog.cpp:299
#, kde-kuit-format
msgctxt "@label"
msgid "Enter new branch name"
msgstr "Introduzca el nombre de la nueva rama"

#: commitdialog.cpp:320
#, kde-kuit-format
msgctxt "@label"
msgid "<b>Branch already exists!</b>"
msgstr "<b>La rama ya existe</b>"

#: commitdialog.cpp:328
#, kde-kuit-format
msgctxt "@label"
msgid "<b>Enter some text!</b>"
msgstr "<b>Introduzca algún texto</b>"

#: commitinfowidget.cpp:38
#, kde-format
msgid ""
"A KDE text-editor component could not be found;\n"
"please check your KDE installation."
msgstr ""
"No fue posible encontrar un componente de edición de texto de KDE\n"
"Compruebe su instalación de KDE."

#: configdialog.cpp:28
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Repository Configuration"
msgstr "Configuración del repositorio de <application>Hg</application>"

#: configdialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Global Configuration"
msgstr "Configuración global de <application>Hg</application>"

#: configdialog.cpp:44
#, kde-kuit-format
msgctxt "@label:group"
msgid "General Settings"
msgstr "Preferencias generales"

#: configdialog.cpp:48
#, kde-kuit-format
msgctxt "@label:group"
msgid "Repository Paths"
msgstr "Rutas de repositorios"

#: configdialog.cpp:51
#, kde-kuit-format
msgctxt "@label:group"
msgid "Ignored Files"
msgstr "Archivos ignorados"

#: configdialog.cpp:55
#, kde-kuit-format
msgctxt "@label:group"
msgid "Plugin Settings"
msgstr "Preferencias de complementos"

#: createdialog.cpp:23
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Initialize Repository"
msgstr "Inicializar repositorio de <application>Hg</application>"

#: createdialog.cpp:24
#, kde-kuit-format
msgctxt "@action:button"
msgid "Initialize Repository"
msgstr "Inicializar el repositorio"

#: createdialog.cpp:59
#, kde-kuit-format
msgctxt "error message"
msgid "Error creating repository!"
msgstr "Ha ocurrido un error al crear el repositorio"

#: exportdialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "<application>Hg</application> Export"
msgstr "Exportación de <application>Hg</application>"

#: exportdialog.cpp:29
#, kde-kuit-format
msgctxt "@action:button"
msgid "Export"
msgstr "Exportar"

#: exportdialog.cpp:56
#, kde-format
msgctxt "@label"
msgid "Treat all files as text"
msgstr "Tratar todos los archivos como texto"

#: exportdialog.cpp:57
#, kde-format
msgctxt "@label"
msgid "Use Git extended diff format"
msgstr "Usar el formato de diferencias extendido de Git"

#: exportdialog.cpp:58
#, kde-format
msgctxt "@label"
msgid "Omit dates from diff headers"
msgstr "Omitir fechas de las cabeceras de diferencias"

#: exportdialog.cpp:127
#, kde-format
msgctxt "@message:error"
msgid "Please select at least one changeset to be exported!"
msgstr "Seleccione al menos un conjunto de cambios a exportar."

#: fileviewhgplugin.cpp:94
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Add"
msgstr "Adición de <application>Hg</application>"

#: fileviewhgplugin.cpp:101
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Remove"
msgstr "Eliminación de <application>Hg</application>"

#: fileviewhgplugin.cpp:108
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Rename"
msgstr "Cambio de nombre de <application>Hg</application>"

#: fileviewhgplugin.cpp:115
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Commit"
msgstr "Envío de <application>Hg</application>"

#: fileviewhgplugin.cpp:122
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Tag"
msgstr "Etiquetado de <application>Hg</application>"

#: fileviewhgplugin.cpp:129
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Branch"
msgstr "Rama de <application>Hg</application>"

#: fileviewhgplugin.cpp:136
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Clone"
msgstr "Clonación de <application>Hg</application>"

#: fileviewhgplugin.cpp:143
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Init"
msgstr "Iniciar repositorio de <application>Hg</application>"

#: fileviewhgplugin.cpp:150
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Update"
msgstr "Actualización de <application>Hg</application>"

#: fileviewhgplugin.cpp:157
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Global Config"
msgstr "Configuración global de <application>Hg</application>"

#: fileviewhgplugin.cpp:164
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Repository Config"
msgstr "Configuración del repositorio de <application>Hg</application>"

#: fileviewhgplugin.cpp:171
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Push"
msgstr "Incorporación de <application>Hg</application>"

#: fileviewhgplugin.cpp:178
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Pull"
msgstr "Extracción de <application>Hg</application>"

#: fileviewhgplugin.cpp:185
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Revert"
msgstr "Reversión de <application>Hg</application>"

#: fileviewhgplugin.cpp:192
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Revert All"
msgstr "Revertir todo de <application>Hg</application>"

#: fileviewhgplugin.cpp:199
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Rollback"
msgstr "Anulación de <application>Hg</application>"

#: fileviewhgplugin.cpp:206
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Merge"
msgstr "Fusión de <application>Hg</application>"

#: fileviewhgplugin.cpp:213
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Bundle"
msgstr "Empaquetado de <application>Hg</application>"

#: fileviewhgplugin.cpp:220
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Export"
msgstr "Exportación de <application>Hg</application>"

#: fileviewhgplugin.cpp:227
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Import"
msgstr "Importación de <application>Hg</application>"

#: fileviewhgplugin.cpp:234
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Unbundle"
msgstr "Desempaquetado de <application>Hg</application>"

#: fileviewhgplugin.cpp:241
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Serve"
msgstr "Servidor de <application>Hg</application>"

#: fileviewhgplugin.cpp:248
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Backout"
msgstr "Deshacer de <application>Hg</application>"

#: fileviewhgplugin.cpp:255
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Diff"
msgstr "Diferencias de <application>Hg</application>"

#: fileviewhgplugin.cpp:279
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Mercurial</application>"
msgstr "<application>Mercurial</application>"

#: fileviewhgplugin.cpp:489
#, kde-kuit-format
msgctxt "@info:status"
msgid "Adding files to <application>Hg</application> repository..."
msgstr "Añadiendo archivos al repositorio de <application>Hg</application>..."

#: fileviewhgplugin.cpp:491
#, kde-kuit-format
msgctxt "@info:status"
msgid "Adding files to <application>Hg</application> repository failed."
msgstr "Fallo al añadir archivos al repositorio <application>Hg</application>."

#: fileviewhgplugin.cpp:493
#, kde-kuit-format
msgctxt "@info:status"
msgid "Added files to <application>Hg</application> repository."
msgstr "Archivos añadidos al repositorio <application>Hg</application>."

#: fileviewhgplugin.cpp:506
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to remove selected files from the repository?"
msgstr "¿Desea eliminar los archivos seleccionados del repositorio?"

#: fileviewhgplugin.cpp:507
#, kde-format
msgid "Remove Files"
msgstr "Eliminar archivos"

#: fileviewhgplugin.cpp:513
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removing files from <application>Hg</application> repository..."
msgstr "Eliminando archivos del repositorio <application>Hg</application>..."

#: fileviewhgplugin.cpp:515
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removing files from <application>Hg</application> repository failed."
msgstr ""
"Fallo al eliminar archivos del repositorio <application>Hg</application>."

#: fileviewhgplugin.cpp:517
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removed files from <application>Hg</application> repository."
msgstr "Archivos eliminados del repositorio <application>Hg</application>."

#: fileviewhgplugin.cpp:529
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renaming of file in <application>Hg</application> repository failed."
msgstr ""
"El cambio de nombre de archivo en el repositorio <application>Hg</"
"application> ha fallado."

#: fileviewhgplugin.cpp:531
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renamed file in <application>Hg</application> repository successfully."
msgstr ""
"Archivo cambiado de nombre con éxito en el repositorio de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:533
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renaming file in <application>Hg</application> repository."
msgstr ""
"Cambiando nombre de archivo en el repositorio de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:544
#, kde-kuit-format
msgctxt "@message"
msgid "No changes for commit!"
msgstr "No hay cambios a enviar."

#: fileviewhgplugin.cpp:549
#, kde-kuit-format
msgctxt "@info:status"
msgid "Commit to <application>Hg</application> repository failed."
msgstr "El envío al repositorio de <application>Hg</application> ha fallado."

#: fileviewhgplugin.cpp:551
#, kde-kuit-format
msgctxt "@info:status"
msgid "Committed to <application>Hg</application> repository."
msgstr "Enviado al repositorio de <application>Hg</application>."

#: fileviewhgplugin.cpp:553
#, kde-kuit-format
msgctxt "@info:status"
msgid "Commit <application>Hg</application> repository."
msgstr "Enviar repositorio <application>Hg</application>."

#: fileviewhgplugin.cpp:564
#, kde-kuit-format
msgctxt "@info:status"
msgid "Tag operation in <application>Hg</application> repository failed."
msgstr ""
"Ha fallado la operación de etiquetado en el repositorio de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:566
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Tagging operation in <application>Hg</application> repository is successful."
msgstr ""
"La operación de etiquetado en el repositorio de <application>Hg</"
"application> ha tenido éxito."

#: fileviewhgplugin.cpp:568
#, kde-kuit-format
msgctxt "@info:status"
msgid "Tagging operation in <application>Hg</application> repository."
msgstr ""
"Operación de etiquetado en el repositorio de <application>Hg</application>."

#: fileviewhgplugin.cpp:577
#, kde-kuit-format
msgctxt "@info:status"
msgid "Update of <application>Hg</application> working directory failed."
msgstr ""
"Ha fallado la actualización del directorio de trabajo de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:579
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Update of <application>Hg</application> working directory is successful."
msgstr ""
"La actualización del directorio de trabajo de <application>Hg</application> "
"ha resultado exitosa."

#: fileviewhgplugin.cpp:581
#, kde-kuit-format
msgctxt "@info:status"
msgid "Updating <application>Hg</application> working directory."
msgstr ""
"Actualizando el directorio de trabajo de <application>Hg</application>."

#: fileviewhgplugin.cpp:590
#, kde-kuit-format
msgctxt "@info:status"
msgid "Branch operation on <application>Hg</application> repository failed."
msgstr ""
"Ha fallado la operación de rama en el repositorio de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:592
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Branch operation on <application>Hg</application> repository completed "
"successfully."
msgstr ""
"La operación de rama en el repositorio de <application>Hg</application> se "
"ha completado con éxito."

#: fileviewhgplugin.cpp:594
#, kde-kuit-format
msgctxt "@info:status"
msgid "Branch operation on <application>Hg</application> repository."
msgstr "Operación de rama en el repositorio de <application>Hg</application>."

#: fileviewhgplugin.cpp:692
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to revert changes made to selected files?"
msgstr "¿Desea revertir los cambios realizados en los archivos actuales?"

#: fileviewhgplugin.cpp:693 fileviewhgplugin.cpp:714
#, kde-format
msgid "Revert"
msgstr "Revertir"

#: fileviewhgplugin.cpp:699 fileviewhgplugin.cpp:720
#, kde-kuit-format
msgctxt "@info:status"
msgid "Reverting files in <application>Hg</application> repository..."
msgstr ""
"Revirtiendo archivos en el repositorio de <application>Hg</application>..."

#: fileviewhgplugin.cpp:701 fileviewhgplugin.cpp:722
#, kde-kuit-format
msgctxt "@info:status"
msgid "Reverting files in <application>Hg</application> repository failed."
msgstr ""
"Fallo al revertir archivos en el repositorio de <application>Hg</"
"application>."

#: fileviewhgplugin.cpp:703 fileviewhgplugin.cpp:724
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Reverting files in <application>Hg</application> repository completed "
"successfully."
msgstr ""
"La reversión de archivos en el repositorio de <application>Hg</application> "
"se ha completado con éxito."

#: fileviewhgplugin.cpp:713
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to revert all changes made to current working directory?"
msgstr ""
"¿Desea revertir todos los cambios realizados en el directorio de trabajo "
"actual?"

#: fileviewhgplugin.cpp:714
#, kde-format
msgid "Revert All"
msgstr "Revertir todo"

#: fileviewhgplugin.cpp:733
#, kde-kuit-format
msgctxt "@info:status"
msgid "Generating diff for <application>Hg</application> repository..."
msgstr ""
"Generando diferencias para el repositorio de <application>Hg</application>..."

#: fileviewhgplugin.cpp:735
#, kde-kuit-format
msgctxt "@info:status"
msgid "Could not get <application>Hg</application> repository diff."
msgstr ""
"No ha sido posible obtener las diferencias del repositorio de "
"<application>Hg</application>."

#: fileviewhgplugin.cpp:737
#, kde-kuit-format
msgctxt "@info:status"
msgid "Generated <application>Hg</application> diff successfully."
msgstr ""
"Las diferencias de <application>Hg</application> se han generado con éxito."

#: fileviewhgplugin.cpp:767
#, kde-kuit-format
msgctxt "@message:error"
msgid "abort: Uncommitted changes in working directory!"
msgstr "interrumpir: cambios sin enviar en el directorio de trabajo."

#: fileviewhgplugin.cpp:780
#, kde-kuit-format
msgctxt "@info:message"
msgid "No rollback information available!"
msgstr "No hay información disponible para anular."

#: fileviewhgplugin.cpp:792
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to rollback last transaction?"
msgstr "¿Desea anular la última transacción?"

#: fileviewhgplugin.cpp:793
#, kde-format
msgid "Rollback"
msgstr "Anular"

#: fileviewhgplugin.cpp:799
#, kde-kuit-format
msgctxt "@info:status"
msgid "Executing Rollback <application>Hg</application> repository..."
msgstr ""
"Ejecutando anulación en el repositorio de <application>Hg</application>..."

#: fileviewhgplugin.cpp:801
#, kde-kuit-format
msgctxt "@info:status"
msgid "Rollback of <application>Hg</application> repository failed."
msgstr ""
"La anulación del repositorio de <application>Hg</application> ha fallado."

#: fileviewhgplugin.cpp:803
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Rollback of <application>Hg</application> repository completed successfully."
msgstr ""
"La anulación del repositorio de <application>Hg</application> se ha "
"completado con éxito."

#. i18n: ectx: label, entry (commitDialogHeight), group (CommitDialogSettings)
#. i18n: ectx: label, entry (configDialogHeight), group (ConfigDialogSettings)
#. i18n: ectx: label, entry (cloneDialogHeight), group (CloneDialogSettings)
#. i18n: ectx: label, entry (pushDialogBigHeight), group (PushDialogSettings)
#. i18n: ectx: label, entry (pullDialogBigHeight), group (PullDialogSettings)
#. i18n: ectx: label, entry (mergeDialogHeight), group (MergeDialogSettings)
#. i18n: ectx: label, entry (bundleDialogHeight), group (BundleDialogSettings)
#. i18n: ectx: label, entry (exportDialogHeight), group (ExportDialogSettings)
#. i18n: ectx: label, entry (importDialogHeight), group (ImportDialogSettings)
#. i18n: ectx: label, entry (serveDialogHeight), group (ServeDialogSettings)
#. i18n: ectx: label, entry (backoutDialogHeight), group (BackoutDialogSettings)
#: fileviewhgpluginsettings.kcfg:7 fileviewhgpluginsettings.kcfg:27
#: fileviewhgpluginsettings.kcfg:39 fileviewhgpluginsettings.kcfg:49
#: fileviewhgpluginsettings.kcfg:59 fileviewhgpluginsettings.kcfg:69
#: fileviewhgpluginsettings.kcfg:79 fileviewhgpluginsettings.kcfg:89
#: fileviewhgpluginsettings.kcfg:99 fileviewhgpluginsettings.kcfg:109
#: fileviewhgpluginsettings.kcfg:119
#, kde-format
msgid "Dialog height"
msgstr "Altura del diálogo"

#. i18n: ectx: label, entry (commitDialogWidth), group (CommitDialogSettings)
#. i18n: ectx: label, entry (configDialogWidth), group (ConfigDialogSettings)
#. i18n: ectx: label, entry (cloneDialogWidth), group (CloneDialogSettings)
#. i18n: ectx: label, entry (pushDialogBigWidth), group (PushDialogSettings)
#. i18n: ectx: label, entry (pullDialogBigWidth), group (PullDialogSettings)
#. i18n: ectx: label, entry (mergeDialogWidth), group (MergeDialogSettings)
#. i18n: ectx: label, entry (bundleDialogWidth), group (BundleDialogSettings)
#. i18n: ectx: label, entry (exportDialogWidth), group (ExportDialogSettings)
#. i18n: ectx: label, entry (importDialogWidth), group (ImportDialogSettings)
#. i18n: ectx: label, entry (serveDialogWidth), group (ServeDialogSettings)
#. i18n: ectx: label, entry (backoutDialogWidth), group (BackoutDialogSettings)
#: fileviewhgpluginsettings.kcfg:12 fileviewhgpluginsettings.kcfg:32
#: fileviewhgpluginsettings.kcfg:43 fileviewhgpluginsettings.kcfg:53
#: fileviewhgpluginsettings.kcfg:63 fileviewhgpluginsettings.kcfg:73
#: fileviewhgpluginsettings.kcfg:83 fileviewhgpluginsettings.kcfg:93
#: fileviewhgpluginsettings.kcfg:103 fileviewhgpluginsettings.kcfg:113
#: fileviewhgpluginsettings.kcfg:123
#, kde-format
msgid "Dialog width"
msgstr "Ancho del diálogo"

#. i18n: ectx: label, entry (verticalSplitterSizes), group (CommitDialogSettings)
#: fileviewhgpluginsettings.kcfg:17
#, kde-format
msgid "Divides file list and editors with commit details"
msgstr "Divide la lista de archivos y los editores con detalles sobre envíos"

#. i18n: ectx: label, entry (horizontalSplitterSizes), group (CommitDialogSettings)
#: fileviewhgpluginsettings.kcfg:21
#, kde-format
msgid "Divides commit editor and diff editor"
msgstr "Divide el editor de envíos y el editor de diferencias"

#: importdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Import"
msgstr "Importación de <application>Hg</application>"

#: importdialog.cpp:31
#, kde-kuit-format
msgctxt "@action:button"
msgid "Import"
msgstr "Importar"

#: importdialog.cpp:63
#, kde-kuit-format
msgctxt "@label"
msgid "Do not commit, just update the working directory"
msgstr "No enviar, solo actualizar el directorio de trabajo"

#: importdialog.cpp:65
#, kde-kuit-format
msgctxt "@label"
msgid "Skip test for outstanding uncommitted changes"
msgstr "Saltar la prueba para cambios pendientes de envío"

#: importdialog.cpp:67
#, kde-kuit-format
msgctxt "@label"
msgid "Apply patch to the nodes from which it was generated"
msgstr "Aplicar parche a los nodos de los que fue generado"

#: importdialog.cpp:69
#, kde-kuit-format
msgctxt "@label"
msgid "Apply patch without touching working directory"
msgstr "Aplicar parche sin tocar el directorio de trabajo"

#: importdialog.cpp:81
#, kde-kuit-format
msgctxt "@label:button"
msgid "Add Patches"
msgstr "Añadir parches"

#: importdialog.cpp:83
#, kde-kuit-format
msgctxt "@label:button"
msgid "Remove Patches"
msgstr "Eliminar parches"

#: mergedialog.cpp:27
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Merge"
msgstr "Fusión de <application>Hg</application>"

#: mergedialog.cpp:28
#, kde-kuit-format
msgctxt "@label:button"
msgid "Merge"
msgstr "Fusionar"

#: mergedialog.cpp:108
#, kde-kuit-format
msgctxt "@message"
msgid "No head selected for merge!"
msgstr "No se ha seleccionado ningún frente para fusionar."

#: pathselector.cpp:60
#, kde-kuit-format
msgctxt "@label:combobox"
msgid "edit"
msgstr "editar"

#: pulldialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Pull Repository"
msgstr "Extraer repositorio de <application>Hg</application>"

#: pulldialog.cpp:38
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Update to new branch head if changesets were pulled"
msgstr ""
"Actualizar al nuevo frente de rama si se extrajeron conjuntos de cambios"

#: pulldialog.cpp:42
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Force Pull"
msgstr "Forzar extracción"

#: pulldialog.cpp:54
#, kde-kuit-format
msgctxt "@label:group"
msgid "Incoming Changes"
msgstr "Cambios entrantes"

#: pulldialog.cpp:148
#, kde-kuit-format
msgctxt "@message:info"
msgid "No incoming changes!"
msgstr "No hay cambios entrantes"

#: pushdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Push Repository"
msgstr "Incorporar repositorio de <application>Hg</application>"

#: pushdialog.cpp:38
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Allow pushing a new branch"
msgstr "Permitir la incorporación de una nueva rama"

#: pushdialog.cpp:42
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Force Push"
msgstr "Forzar incorporación"

#: pushdialog.cpp:54
#, kde-kuit-format
msgctxt "@label:group"
msgid "Outgoing Changes"
msgstr "Cambios salientes"

#: pushdialog.cpp:170
#, kde-kuit-format
msgctxt "@message:info"
msgid "No outgoing changes!"
msgstr "No hay cambios salientes"

#: renamedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Rename"
msgstr "Cambio de nombre de <application>Hg</application>"

#: renamedialog.cpp:27
#, kde-kuit-format
msgctxt "@action:button"
msgid "Rename"
msgstr "Cambiar nombre"

#: renamedialog.cpp:33
#, kde-kuit-format
msgctxt "@label:label to source file"
msgid "Source "
msgstr "Origen "

#: renamedialog.cpp:39
#, kde-kuit-format
msgctxt "@label:rename"
msgid "Rename to "
msgstr "Cambiar nombre a "

#: servedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Serve"
msgstr "Servidor de <application>Hg</application>"

#: servedialog.cpp:63
#, kde-kuit-format
msgctxt "@label:button"
msgid "Start Server"
msgstr "Iniciar servidor"

#: servedialog.cpp:64
#, kde-kuit-format
msgctxt "@label:button"
msgid "Stop Server"
msgstr "Detener servidor"

#: servedialog.cpp:65
#, kde-kuit-format
msgctxt "@label:button"
msgid "Open in browser"
msgstr "Abrir en el navegador"

#: servedialog.cpp:81
#, kde-kuit-format
msgctxt "@label"
msgid "Port"
msgstr "Puerto"

#: servewrapper.cpp:68
#, kde-format
msgid "## Starting Server ##"
msgstr "## Iniciando el servidor ##"

#: servewrapper.h:151
#, kde-format
msgid "## Server Stopped! ##\n"
msgstr "## Servidor detenido ##\n"

#: statuslist.cpp:30
#, kde-format
msgid "Filename"
msgstr "Nombre de archivo"

#: statuslist.cpp:40
#, kde-format
msgctxt "@title:group"
msgid "File Status"
msgstr "Estado del archivo"

#: syncdialogbase.cpp:82
#, kde-format
msgctxt "@label:button"
msgid "Show Incoming Changes"
msgstr "Mostrar cambios entrantes"

#: syncdialogbase.cpp:86
#, kde-format
msgctxt "@label:button"
msgid "Show Outgoing Changes"
msgstr "Mostrar cambios salientes"

#: syncdialogbase.cpp:120
#, kde-format
msgctxt "@action:button"
msgid "Pull"
msgstr "Extraer"

#: syncdialogbase.cpp:120
#, kde-format
msgctxt "@action:button"
msgid "Push"
msgstr "Incorporar"

#: syncdialogbase.cpp:162
#, kde-format
msgctxt "@message"
msgid "No changes found!"
msgstr "No se han encontrado cambios"

#: syncdialogbase.cpp:213 syncdialogbase.cpp:288 syncdialogbase.cpp:295
#, kde-format
msgid "Error!"
msgstr "Error"

#: syncdialogbase.cpp:329
#, kde-kuit-format
msgctxt "@action:button"
msgid "Options"
msgstr "Opciones"

#: tagdialog.cpp:20
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Tag"
msgstr "Etiquetado de <application>Hg</application>"

#: tagdialog.cpp:30
#, kde-format
msgid "Create New Tag"
msgstr "Crear nueva etiqueta"

#: tagdialog.cpp:31
#, kde-format
msgid "Remove Tag"
msgstr "Eliminar etiqueta"

#: tagdialog.cpp:32
#, kde-format
msgid "Switch Tag"
msgstr "Cambiar etiqueta"

#: tagdialog.cpp:129
#, kde-format
msgid "Created tag successfully!"
msgstr "Etiqueta creada con éxito"

#: updatedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Update"
msgstr "Actualización de <application>Hg</application>"

#: updatedialog.cpp:27
#, kde-kuit-format
msgctxt "@action:button"
msgid "Update"
msgstr "Actualizar"

#: updatedialog.cpp:30
#, kde-format
msgid "New working directory"
msgstr "Nuevo directorio de trabajo"

#: updatedialog.cpp:35
#, kde-format
msgid "Tag"
msgstr "Etiqueta"

#: updatedialog.cpp:36
#, kde-format
msgid "Changeset/Revision"
msgstr "Conjunto de cambios/revisión"

#: updatedialog.cpp:41
#, kde-format
msgid "Current Parent"
msgstr "Padre actual"

#: updatedialog.cpp:47
#, kde-format
msgid "Options"
msgstr "Opciones"

#: updatedialog.cpp:49
#, kde-format
msgid "Discard uncommitted changes"
msgstr "Descartar los cambios pendientes de envío"

#: updatedialog.cpp:126
#, kde-format
msgid ""
"Some error occurred! \n"
"Maybe there are uncommitted changes."
msgstr ""
"Ha ocurrido algún error.\n"
"Es posible que existan cambios sin enviar."
